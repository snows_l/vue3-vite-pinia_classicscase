/*
 * @Description: ------ 文件描述 ------
 * @Creater: snows_l snows_l@163.com
 * @Date: 2022-12-30 10:30:44
 * @LastEditors: snows_l snows_l@163.com
 * @LastEditTime: 2023-12-01 09:09:06
 * @FilePath: /vue3+vite+pinia_classicsCase/vite.config.js
 */
import vue from '@vitejs/plugin-vue';
import path from 'path';
// import { visualizer } from 'rollup-plugin-visualizer';
import { defineConfig, loadEnv } from 'vite';
// https://vitejs.dev/config/
export default defineConfig(({ mode, command }) => {
  console.log('command', command);

  console.log('当前环境的环境', mode); // 当前开发环境 development | production

  // 获取当前环境的环境变量的所有配置（.env.xxxx）
  const config = loadEnv(mode, process.cwd());
  console.log('当前开发环境变量', config); // 当前环境的环境变量的所有配置

  return {
    plugins: [
      vue()
      // visualizer({
      //   open: true, //注意这里要设置为true，否则无效
      //   gzipSize: true,
      //   brotliSize: true
      // })
    ],
    build: {
      rollupOptions: { optimizeDeps: { include: '*' }, input: '/public/index.html' }
    },
    resolve: {
      alias: {
        '@': path.resolve(__dirname, './src')
      }
    },
    server: {
      proxy: {
        '/dev': {
          target: config.VITE_BASIC_URL,
          changeOrigin: true,
          rewrite: path => path.replace(/^\/api/, '')
        }
      }
    }
  };
});
