/*
 * @Description: ------------ 初始化全局配置 -----------
 * @Author: snows_l snows_l@163.com
 * @Date: 2023-05-30 16:28:08
 * @LastEditors: snows_l snows_l@163.com
 * @LastEditTime: 2023-12-04 15:13:16
 * @FilePath: /vue3+vite+pinia_classicsCase/src/init.ts
 */

// 本地存在全局配置就更新到pinia中
import { usePGCStore } from '@/store/projectGloabalConfig';
import local from '@/utils/local';
export default () => {
  const projectGloabalConfig = local.get('PGC');
  if (projectGloabalConfig) {
    usePGCStore().updateConfig(projectGloabalConfig);
  }
};
