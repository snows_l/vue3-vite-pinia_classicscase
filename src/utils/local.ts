/*
 * @Description: ------------ fileDescription -----------
 * @Author: snows_l snows_l@163.com
 * @Date: 2023-05-30 15:51:37
 * @LastEditors: snows_l snows_l@163.com
 * @LastEditTime: 2023-05-30 16:54:27
 * @FilePath: /my-vue3-vite-app/src/utils/local.ts
 */
const prefix = 'x-';
const local = {
  // 取
  get(key: string) {
    if (window.localStorage.getItem(prefix + key)) {
      return JSON.parse(window.localStorage.getItem(prefix + key) || '{}');
    } else {
      return undefined;
    }
  },

  // 存
  set(key: string, value: any) {
    if (Object.prototype.toString.call(value).slice(8, -1) == 'String') {
      window.localStorage.setItem(prefix + key, value);
    } else {
      window.localStorage.setItem(prefix + key, JSON.stringify(value));
    }
  },

  // 删
  del(key: string) {
    window.localStorage.removeItem(prefix + key);
  },

  // 清
  clear() {
    window.localStorage.clear();
  }
};

export default local;
