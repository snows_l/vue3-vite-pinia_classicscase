/*
 * @Description: ------------ 项目的全局配置 -----------
 * @Author: snows_l snows_l@163.com
 * @Date: 2023-05-26 15:02:31
 * @LastEditors: snows_l snows_l@163.com
 * @LastEditTime: 2023-05-30 10:20:36
 * @FilePath: /my-vue3-vite-app/src/project.config.js
 */

/**
 * @description:  项目的全局配置
 * @param { String } dialogOrDrawer  dialog | drawer
 */
export default {
  dialogOrDrawer: 'dialog'
};
