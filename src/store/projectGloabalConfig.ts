/*
 * @Description: ------------ 全局配置 -----------
 * @Author: snows_l snows_l@163.com
 * @Date: 2023-05-30 10:33:23
 * @LastEditors: snows_l snows_l@163.com
 * @LastEditTime: 2023-12-04 12:32:20
 * @FilePath: /vue3+vite+pinia_classicsCase/src/store/projectGloabalConfig.ts
 */
import local from '@/utils/local';
import { defineStore } from 'pinia';
export const usePGCStore = defineStore('projectGlobalConfig', {
  // 推荐使用 完整类型推断的箭头函数
  state: () => {
    return {
      // 所有这些属性都将自动推断其类型
      dialogOrDrawer: 'dialog',
      themeColor: '#eeeeee',
      isCrumb: true, // 是否展示面包屑
      isFooter: true // 是否展footer
    };
  },
  actions: {
    //
    changeDialogOrDrawer(param: string) {
      this.dialogOrDrawer = param;
    },

    // 更新传过来的数据
    updateConfig(param: { [key: string]: any }): void {
      const keys = Object.keys(param);
      keys.forEach(item => {
        (this as any)[item] = param[item];
      });
      this.persistence();
    },

    // 数据持久化
    persistence(): void {
      local.set('PGC', this.$state);
    }
  }
});
