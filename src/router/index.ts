/*
 * @Description: ------------ fileDescription -----------
 * @Author: snows_l snows_l@163.com
 * @Date: 2023-01-12 16:56:04
 * @LastEditors: snows_l snows_l@163.com
 * @LastEditTime: 2023-12-04 15:52:45
 * @FilePath: /vue3+vite+pinia_classicsCase/src/router/index.ts
 *
 */

import Layout from '../layout/index.vue';
import Index from '../views/index/index.vue';
import Login from '../views/Login/index.vue';

import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router';

// 菜单路由
export const whiteRouters: Array<RouteRecordRaw> = [
  {
    path: '/skill',
    name: 'Utils',
    redirect: '/skill/dialog',
    meta: {
      title: '小技巧'
    },
    children: [
      {
        path: '/skill/dialog',
        name: 'Dialog',
        component: () => import('../views/Dialog/index.vue'),
        meta: {
          title: 'Dialog'
        }
      },
      {
        path: '/skill/script',
        name: 'Script',
        title: 'Script',
        component: () => import('../views/Script/index.vue'),
        meta: {
          title: 'Script'
        }
      },
      {
        path: '/skill/base64',
        name: 'Base64',
        title: 'Base64',
        component: () => import('../views/Base64/index.vue'),
        meta: {
          title: 'Base64'
        }
      },
      {
        path: '/skill/vueuse',
        name: 'Vueuse',
        title: 'Vueuse',
        component: () => import('../views/Vueuse/index.vue'),
        meta: {
          title: 'Vueuse'
        }
      },
      {
        path: '/skill/router',
        name: 'Router',
        title: 'Router',
        component: () => import('../views/RouterRoute/index.vue'),
        meta: {
          title: 'Router'
        }
      },
      {
        path: '/skill/charts',
        name: 'Charts',
        title: 'Charts',
        component: () => import('../views/Charts/index.vue'),
        meta: {
          title: 'Charts'
        }
      }
    ]
  },
  {
    path: '/menu',
    name: 'Menu',
    title: 'Menu',
    meta: {
      title: 'Menu'
    },
    children: [
      {
        path: '/menu/menu1',
        name: 'menu1',
        title: 'menu1',
        component: () => import('../views/Menu/index.vue'),
        meta: {
          title: 'menu1'
        }
      },
      {
        path: '/menu/menu2',
        name: 'menu2',
        title: 'menu2',
        meta: {
          title: 'Menu2'
        },
        children: [
          {
            path: '/menu/menu2/menu21',
            name: 'menu21',
            title: 'menu21',
            component: () => import('../views/Menu/menu21.vue'),
            meta: {
              title: 'menu21'
            }
          },
          {
            path: '/menu/menu2/menu22',
            name: 'menu22',
            title: 'menu22',
            component: () => import('../views/Menu/menu22.vue'),
            meta: {
              title: 'menu22'
            },
            children: [
              {
                path: '/menu/menu2/menu22/menu221',
                name: 'menu221',
                title: 'menu221',
                component: () => import('../views/Menu/menu221.vue'),
                meta: {
                  title: 'menu221'
                }
              },
              {
                path: '/menu/menu2/menu22/menu222',
                name: 'menu222',
                title: 'menu222',
                component: () => import('../views/Menu/menu222.vue'),
                meta: {
                  title: 'menu222'
                },
                children: [
                  {
                    path: '/menu/menu2/menu22/menu222/menu2221',
                    name: 'menu2221',
                    component: () => import('../views/Menu/menu221.vue'),
                    meta: {
                      title: 'menu2221'
                    }
                  }
                ]
              }
            ]
          }
        ]
      }
    ]
  }
];
const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'Index',
    component: Index
  },

  {
    path: '/login',
    name: 'Login',
    component: Login
  },
  {
    path: '/big-screen',
    name: 'BigScreen',
    component: () => import('../views/bigScreen/index.vue')
  },
  {
    path: '/register',
    name: 'Register',
    component: () => import('../views/Register/index.vue')
  },
  {
    path: '/view',
    name: 'View',
    redirect: '/skill/dialog',
    component: Layout,
    children: whiteRouters
  }
];
const router = createRouter({
  history: createWebHistory(),
  routes
});

// router.addRoute()

router.beforeEach((to, from, next) => {
  next();
});

export default router;
