/*
 * @Description: ------------ fileDescription -----------
 * @Author: snows_l snows_l@163.com
 * @Date: 2023-01-12 16:56:13
 * @LastEditors: snows_l snows_l@163.com
 * @LastEditTime: 2023-12-04 15:34:23
 * @FilePath: /vue3+vite+pinia_classicsCase/src/main.js
 */

import '@/assets/css/base.scss';
import '@/assets/css/reset.scss';
import ElementPlus, { ElMessage } from 'element-plus';
import 'element-plus/dist/index.css';
import zhCn from 'element-plus/dist/locale/zh-cn.mjs';
import { createApp } from 'vue';

import App from './App.vue';
import init from './init';
import projectGlobalConfig from './project.config.js';
import router from './router';
console.log('main页面', import.meta.env);
console.log('是不是开发环境', import.meta.env.DEV);
console.log('是不是正式环境', import.meta.env.PROD);

import * as ElementPlusIconsVue from '@element-plus/icons-vue';

import pinia from '@/store';
const app = createApp(App);

// 注册el-icon图标
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
  app.component(key, component);
}

app.use(pinia);
app.use(ElementPlus, {
  locale: zhCn
});
app.use(router);
app.mount('#app');
init();

// 项目的全局配置 ==> 挂在到vue原型上
app.config.globalProperties.$projectGlobalConfig = projectGlobalConfig;

window.addEventListener('visibilitychange', () => {
  // document.hidden; // true:离开   false:回来
  // document.visibilityState; // hidden:离开   visible:回来
  if (!document.hidden) {
    document.title = '都已经是第二年的立春、又何必执着于、当年的盛​夏呢？';
    ElMessage.success('欢迎您回来！');
  } else {
    document.title = '滚回来看文档！';
  }
});
